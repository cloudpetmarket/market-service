package ru.simple.marketservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.simple.marketservice.domain.Product;
import ru.simple.marketservice.domain.ProductGroup;
import ru.simple.marketservice.repository.ProductGroupRepository;
import ru.simple.marketservice.repository.ProductRepository;

import java.util.List;

@RestController
@RequestMapping ("/admin")
@RequiredArgsConstructor
public class AdminApiController {
    private final ProductRepository productRepository;
    private final ProductGroupRepository productGroupRepository;

    @GetMapping ("/products")
    public Iterable<Product> getAllProducts () {
        return productRepository.findAll();
    }

    @PostMapping ("/products")
    public Product createProduct (@RequestBody Product product) {
        return productRepository.save(product);
    }

    @PostMapping ("/group")
    public ProductGroup createGroup (@RequestBody ProductGroup productGroup) {
        return productGroupRepository.save(productGroup);
    }

}
