package ru.simple.marketservice.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document
public class Product {
    @Id
    private String productId;

    private String productName;

    @DBRef
    private ProductGroup productGroup;
}
