package ru.simple.marketservice.domain;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document
public class ProductGroup {
    @Id
    private String productGroupId;

    private String productGroupName;
}
