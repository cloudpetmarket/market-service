package ru.simple.marketservice.repository;


import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.simple.marketservice.domain.ProductGroup;

@Repository
public interface ProductGroupRepository extends PagingAndSortingRepository<ProductGroup, String> {
}
