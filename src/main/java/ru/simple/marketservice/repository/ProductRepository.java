package ru.simple.marketservice.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.simple.marketservice.domain.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, String> {


}
